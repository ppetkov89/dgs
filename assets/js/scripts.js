// Nav menu toggle.
$('.menu-mobile-icon').click(function() {
  $('.navigation').toggleClass('open');
  $('body').toggleClass('menu-open');
});

// Session hover effect.
$('.session-list1 li').on('mouseenter', function () {
  var img_src = $(this).data('img_path');
  $(this).parents('.session-list').find('.section-image img').attr('src', img_src);
})
.on('mouseleave', function () {
  var default_img = $(this).parents('.session-list').find('.section-image img');
  default_img.attr('src', default_img.data('default_path'));
});

// Navigation scroll.
$('.navigation a').on('click', function (e) {
  var section = $(this).data('section');
  if(section){
    e.preventDefault();
    var section_offset = $('.' + section).offset().top;
    var nav_height = $('.navigation').outerHeight();
    $('.navigation').removeClass('open');
    $('body').removeClass('menu-open');

    $("HTML, BODY").animate({
         scrollTop: section_offset - nav_height
    }, 1000);
  }
});

// Resize full width image container.
$('.section-image--part').height($('.section-image--part img').height());

$(window).resize(function() {
  $('.section-image--part').height($('.section-image--part img').height());
})

/**
 * Trigger a callback when the selected images are loaded:
 * @param {String} selector
 * @param {Function} callback
 */
var onImgLoad = function(selector, callback){
  $(selector).each(function(){
    if (this.complete || /*for IE 10-*/ $(this).height() > 0) {
      callback.apply(this);
    }
    else {
      $(this).on('load', function(){
        callback.apply(this);
      });
    }
  });
};

onImgLoad('.section-image--part img', function(){
  $('.section-image--part').height($('.section-image--part img').height());
});

$(window).scroll(function() {
		var scrollDistance = $(window).scrollTop();

		$('section').each(function(i) {
				if ($(this).position().top <= scrollDistance + 100) {
					$('.sticky li.active').removeClass('active');
					$('.sticky li').eq(i).addClass('active');

          $('section.active-section').removeClass('active-section');
          $(this).addClass('active-section');

				}

        if ($(this).position().top <= scrollDistance + 500) {
          $(this).find('.animate').each(function () {
            if (!$(this).hasClass('animated')) {
              $(this).addClass('animated');
              $(this).addClass($(this).data('animation'));
            }
          });
        }
		});

    if (($(window).height() + scrollDistance) >= $('body').height()) {
      $('.sticky li.active').removeClass('active');
      $('.sticky li:last-child').addClass('active');
    }

    if (scrollDistance < 150) {
      $('.sticky').removeClass('bg-overlay');
    } else {
      $('.sticky').addClass('bg-overlay');
    }
}).scroll();
